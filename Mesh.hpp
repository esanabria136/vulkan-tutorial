#if __linux__ || __linux || __gnu_linux__
#ifndef MESH_H_
#define MESH_H_

#include <vulkan/vulkan.h>
#include <vector>
#include "Utilities.hpp"

struct UboModel
{
	glm::mat4 model;
};

class Mesh
{
public:
	Mesh();
	Mesh(	VkPhysicalDevice newphysicalDevice,
			VkDevice newDevice,
			VkQueue transferQueue,
			VkCommandPool transferCommandPool,
			std::vector<Vertex> *vertices,
			std::vector<uint32_t> *indices);


	void setModel(glm::mat4 newModel);
	UboModel getModel();

	uint32_t getVertexCount();
	VkBuffer getVertexBuffer();

	uint32_t getIndexCount();
	VkBuffer getIndexBuffer();

	void destroyBuffers();

	~Mesh();

private:
	UboModel uboModel;
	uint32_t vertexCount;
	VkBuffer vertexBuffer;
	VkDeviceMemory vertexBufferMemory;

	uint32_t indexCount;
	VkBuffer indexBuffer;
	VkDeviceMemory indexBufferMemory;

	VkPhysicalDevice physicalDevice;
	VkDevice device;

	void createVertexBuffer(VkQueue transferQueue, VkCommandPool transferCommandPool, std::vector<Vertex> *vertices);
	void createIndexBuffer(VkQueue transferQueue, VkCommandPool transferCommandPool, std::vector<uint32_t> *indices);
};

#endif /* MESH_H_ */
#elif
#error Platform not supported!
#endif /* __linux__ */