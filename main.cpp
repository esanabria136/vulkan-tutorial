//gcc -dM -E - </dev/null
#if __linux__ || __linux || __gnu_linux__

#include <iostream>
#include <iomanip>
#include <string>
#include <SDL2/SDL.h>
#include "gamemode_client.h"
#include "vulkan/vulkan.h"
#include "VulkanRenderer.hpp"

int main()
{
	int width{ 800 };
	int height{ 600 };
	int width2{ width };
	int height2{ height };
	const unsigned short increment{ 10u };
	bool isClosed{ false };
	bool animate{ true };
	SDL_Window *window{ nullptr };
	std::string title = "Vulkan";

	VulkanRenderer *CVulkan = new VulkanRenderer;

	if(int err{ gamemode_request_start() }; err != 0)
	{
		std::cerr << "\n\tFailed to start Feral Gamemode[" << gamemode_error_string() << "]!" << std::endl << std::flush;
	}

	SDL_Init(SDL_INIT_EVERYTHING);

	window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE);

	if(int r{ CVulkan->init(window) }; r != 0)
	{
		std::cerr <<"\n\tFailed Vulkan init" << std::endl;
	}
	
	float angle{ 0.0f };
	Uint32 windowID = SDL_GetWindowID(window);
	std::chrono::duration<float> deltaTime;
	std::chrono::system_clock::time_point lastTime{ std::chrono::system_clock::now() };

	while(!isClosed)
	{
		SDL_Event e;

		if(animate)
		{
			std::chrono::system_clock::time_point now{ std::chrono::system_clock::now() };
			deltaTime = now - lastTime;
			lastTime = now;

			angle += 10.0f * deltaTime.count();

			if(angle > 360.0f)
			{
				angle -= 360.0f;
			}
		}
		
		while(SDL_PollEvent(&e))
		{
			if((e.type == SDL_QUIT) || (e.key.keysym.scancode == SDL_SCANCODE_Q))
			{
				isClosed = true;
				break;
			}

			switch (e.type)
			{
			case SDL_WINDOWEVENT:
				if(e.window.windowID == windowID)
				{				
					switch(e.window.event)
					{
					case SDL_WINDOWEVENT_SIZE_CHANGED:	//6
					{
						int nWidth = e.window.data1;
						int nHeight = e.window.data2;
						CVulkan->resizeWindow(nWidth, nHeight);
					}
					break;

					case SDL_WINDOWEVENT_RESIZED:		//5
					{
						int nWidth = e.window.data1;
						int nHeight = e.window.data2;
						CVulkan->resizeWindow(nWidth, nHeight);
					}
					break;

					default:
						break;
					}
				}

			break;

			case SDL_KEYDOWN:
				if(e.key.keysym.scancode == SDL_SCANCODE_P)
				{
					animate = !animate;
				}
				else if(e.key.keysym.scancode == SDL_SCANCODE_Q)
				{
					isClosed = true;
				}
				else if(e.key.keysym.scancode == SDL_SCANCODE_A)
				{
					printf("\n\t---> [ SDL_SCANCODE_A ] <---\n");
					fflush(stdout);
					width2 += increment;
					height2 += increment;
					//SDL_SetWindowSize(window, width2, height2);
					CVulkan->resizeWindow(width2, height2);
				}
				else if(e.key.keysym.scancode == SDL_SCANCODE_D)
				{
					printf("\n\t---> [ SDL_SCANCODE_D ] <---\n");
					fflush(stdout);
					width2 -= increment;
					height2 -= increment;
					//SDL_SetWindowSize(window, width2, height2);
					CVulkan->resizeWindow(width2, height2);
				}

			break;
			
			default:
				break;
			}
		}

		if(animate)
		{
			glm::mat4 firstModel(1.0f);
			glm::mat4 secondModel(1.0f);

			firstModel = glm::translate(firstModel, glm::vec3(-2.0f, 0.0f, -5.0f));
			//firstModel = glm::rotate(firstModel, glm::radians(angle), glm::vec3(0.0f, 0.0f, 1.0f));

			secondModel = glm::translate(secondModel, glm::vec3(2.0f, 0.0f, -5.0f));
			//secondModel = glm::rotate(secondModel, glm::radians(-angle * 10), glm::vec3(0.0f, 0.0f, 1.0f));

			CVulkan->updateModel(0, firstModel);
			CVulkan->updateModel(1, secondModel);
		}

		CVulkan->draw();
	}

	CVulkan->cleanup();

	delete CVulkan;
	CVulkan = nullptr;
	SDL_DestroyWindow(window);
	window = nullptr;
	SDL_Quit();

	std::cout << "\n\n" << std::flush;

	if(int err{ gamemode_request_end() }; err != 0)
	{
		std::cerr << "\n\tUnable to end Feral Gamemode[" << gamemode_error_string() <<"]!\n" << std::endl << std::flush;
	}

	return 0;
}
#elif
#error Platform not supported!
#endif /* __linux__ */