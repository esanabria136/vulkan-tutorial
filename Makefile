.PHONY: all

CC = gcc
CPP = g++
OUT = exec
OUTD = ${OUT}-debug
FLAGS = -std=c++20 -march=native -Wno-return-type -Wno-narrowing
LIBS = -ldl -lSDL2 -lvulkan -L${VULKAN_SDK}/lib
INCL = -I${VULKAN_SDK}/include -I /home/erik/gamemode/lib -include/home/erik/gamemode/lib/gamemode_client.h
SRC := $(wildcard *.cpp) $(wildcard *.hpp)
#SRCS := $(wildcard *.cpp)
#-std=c++17 -ISDL2 -IGLEW -IGL -O3 -c -fmessage-length=0 -march=native -fexceptions
# LD_LIBRARY_PATH=$(VULKAN_SDK)/lib

all:
	${CPP} ${FLAGS} ${INCL} -O3 ${SRC} -o ${OUT} ${LIBS}

debug:
	${CPP} ${FLAGS} ${INCL} ${SRC} -g3 -D DBUG -o ${OUTD} ${LIBS}

clean:
	rm -v ${OUT}
	rm -v ${OUTD}