#if __linux__ || __linux || __gnu_linux__

#include "VulkanRenderer.hpp"
#include "VulkanValidation.hpp"
#include <SDL2/SDL_vulkan.h>

VulkanRenderer::VulkanRenderer() :
	m_window(nullptr),
	m_start(std::chrono::system_clock::now()),
	m_end(std::chrono::system_clock::now())
{

}

int VulkanRenderer::init(SDL_Window *w)
{
	m_window = w;

	try
	{
		createInstance();
		createDebugCallback();
		createSurface();
		getPhysicalDevice();
		createLogicalDevice();
		createSwapChain();
		createRenderPass();
		createDescriptorSetLayout();
		createGraphicsPipeline();
		createFramebuffers();
		createCommandPool();
		
		uboViewProjection.projection  = glm::perspective(glm::radians(45.0f), static_cast<float>(swapChainExtent.width) / static_cast<float>(swapChainExtent.height), 0.1f, 100.0f);
		uboViewProjection.view = glm::lookAt(glm::vec3(0.0f, 0.0f, 3.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		uboViewProjection.projection[1][1] *= -1;

		//create a mesh
		std::vector<Vertex> meshVertices = {
				{{0.4, -0.4, 0.0}, {1.0f, 0.0f, 0.0f}},
				{{0.4, 0.4, 0.0}, {0.0f, 1.0f, 0.0f}},
				{{-0.4, 0.4, 0.0}, {0.0f, 0.0f, 1.0f}},
				{{-0.4, -0.4, 0.0}, {1.0f, 1.0f, 0.0f}}
		};

		std::vector<Vertex> meshVertices2 = {
				{{0.4, -0.4, 0.0}, {1.0f, 0.0f, 0.0f}},
				{{0.4, 0.4, 0.0}, {0.0f, 1.0f, 0.0f}},
				{{-0.4, 0.4, 0.0}, {0.0f, 0.0f, 1.0f}},
				{{-0.4, -0.4, 0.0}, {1.0f, 1.0f, 0.0f}}
		};

		//Index data
		std::vector<uint32_t> meshIndices =
		{
				0, 1, 2,
				2, 3, 0
		};

		Mesh firstMesh(mainDevice.physicalDevice, mainDevice.logicalDevice, graphicsQueue, graphicsCommandPool, &meshVertices, &meshIndices);
		Mesh secondMesh(mainDevice.physicalDevice, mainDevice.logicalDevice, graphicsQueue, graphicsCommandPool, &meshVertices2, &meshIndices);

		meshList.push_back(firstMesh);
		meshList.push_back(secondMesh);
/* 
		glm::mat4 meshModelMatrix = meshList.at(0).getModel().model;
		meshModelMatrix = glm::rotate(meshModelMatrix, glm::radians(45.0f), glm::vec3(0.0f, 0.0f, 1.0f));
		meshList.at(0).setModel(meshModelMatrix);
 */
		createCommandBuffers();
		allocateDynamicBufferTransferSpace();
		createUniformBuffers();
		createDescriptorPool();
		createDescriptorSets();
		recordCommands();
		createSynchronisation();
	}
	catch(const std::runtime_error& e)
	{
		std::cerr <<"\n\tRuntime ERROR: "<< e.what() << std::endl;
		return 1;
	}
	
	return 0;
}

void VulkanRenderer::resizeWindow(int w, int h)
{
	windowResized = true;
	newExtent.width = w;
	newExtent.height = h;
	std::cout <<"\n\t---> [ VulkanRenderer::resizeWindow: " << w <<", " << h <<" ] <---\n" << std::flush;
	SDL_SetWindowSize(m_window, w, h);
	recreateSwapChain();
	//windowResized = false;
}

void VulkanRenderer::updateModel(size_t modelID, glm::mat4 newModel)
{
	if(modelID >= meshList.size())
	{
		return;
	}

	meshList.at(modelID).setModel(newModel);
}

void VulkanRenderer::draw()
{
	//1. Get next available image to draw to and set something to signal when we're finished with the image (semaphore)
	//2. Submit our command buffer to queue for execution, making sure it waits for the image to be signalled as
	// available for drawing and signals when it has finished rendreing
	//3. Present image to screen when it has signalled finished rendering

	// -- Get next Image --
	//wait for given fence to signal (open) from las draw before continuing
	vkWaitForFences(mainDevice.logicalDevice, 1, &drawFences.at(currentFrame), VK_TRUE, std::numeric_limits<uint64_t>::max());
	//Manually reset fences (close)
	vkResetFences(mainDevice.logicalDevice, 1, &drawFences.at(currentFrame));

	uint32_t imageIndex{ 0 };
	//VkResult resA = vkAcquireNextImageKHR(mainDevice.logicalDevice, swapchain, std::numeric_limits<uint64_t>::max(), imageAvailable.at(currentFrame), VK_NULL_HANDLE, &imageIndex);

	if(VkResult resA = vkAcquireNextImageKHR(mainDevice.logicalDevice, swapchain, std::numeric_limits<uint64_t>::max(), imageAvailable.at(currentFrame), VK_NULL_HANDLE, &imageIndex); resA == VK_ERROR_OUT_OF_DATE_KHR)
	{
		std::cout <<"\n\t---> [ VulkanRenderer::draw: " << __LINE__ <<", VK_ERROR_OUT_OF_DATE_KHR ] <---" << std::endl << std::flush;

		recreateSwapChain();

		return;
	}
	else if(resA != VK_SUCCESS && resA != VK_SUBOPTIMAL_KHR)
	{
		throw std::runtime_error("Failed vkAcquireNextImageKHR: " + resA);
	}

	// Only reset the fence if we are submitting work
	vkResetFences(mainDevice.logicalDevice, 1, &drawFences.at(currentFrame));

	updateUniformBufers(imageIndex);

	// -- Submit Command Buffer to Render --
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = &imageAvailable.at(currentFrame);
	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
	submitInfo.pWaitDstStageMask = waitStages;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffers.at(imageIndex);
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = &renderFinished.at(currentFrame);

	if(VkResult res = vkQueueSubmit(graphicsQueue, 1, &submitInfo, drawFences.at(currentFrame)); res != VK_SUCCESS)
	{
		throw std::runtime_error("Failed to submit command buffer to queue: " + res);
	}

	// -- Present rendered image to screen --
	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = &renderFinished.at(currentFrame);
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = &swapchain;
	presentInfo.pImageIndices = &imageIndex;

	if(VkResult res = vkQueuePresentKHR(presentationQueue, &presentInfo); (res == VK_ERROR_OUT_OF_DATE_KHR) || (res == VK_SUBOPTIMAL_KHR))
	{
		std::cout <<"\n\t---> [ VulkanRenderer::draw: " << __LINE__ <<", VK_ERROR_OUT_OF_DATE_KHR, VK_SUBOPTIMAL_KHR ] <---" << std::endl << std::flush;

		recreateSwapChain();
	}
	else if( res != VK_SUCCESS)
	{
		throw std::runtime_error("Failed to present Image: " + res);
	}

	currentFrame = (currentFrame + 1) % MAX_FRAME_DRAWS;

	//FPS
	m_end = std::chrono::system_clock::now();
	std::chrono::duration<float> m_diff = m_end - m_start;
	m_start = m_end;
	float md_diff{ m_diff.count() };
	char info[128];
	snprintf(info, 128, "Vulkan: FPS: %4.2f", 1.0 / md_diff);
	SDL_SetWindowTitle(m_window, info);
}

void VulkanRenderer::cleanup()
{
	//wait until the device is not doing anything,
	//nothing left in the queue
	vkDeviceWaitIdle(mainDevice.logicalDevice);

	free(modelTransferSpace);

	cleanupSwapChain();

	/*vkDestroyDescriptorPool(mainDevice.logicalDevice, descriptorPool, nullptr);

	for(size_t i{ 0 }; i < swapChainImages.size(); ++i)
	{
		vkDestroyBuffer(mainDevice.logicalDevice, vpUniformBuffer.at(i), nullptr);
		vkFreeMemory(mainDevice.logicalDevice, vpUniformBufferMemory.at(i), nullptr);

		vkDestroyBuffer(mainDevice.logicalDevice, modelDUniformBuffer.at(i), nullptr);
		vkFreeMemory(mainDevice.logicalDevice, modelDUniformBufferMemory.at(i), nullptr);
	}*/

	vkFreeCommandBuffers(mainDevice.logicalDevice, graphicsCommandPool, static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data());
	vkDestroyCommandPool(mainDevice.logicalDevice, graphicsCommandPool, nullptr);

	for(size_t i{ 0 }; i < meshList.size(); ++i)
	{
		meshList.at(i).destroyBuffers();
	}

	for(size_t i{ 0 }; i < MAX_FRAME_DRAWS; ++i)
	{
		vkDestroySemaphore(mainDevice.logicalDevice, renderFinished.at(i), nullptr);
		vkDestroySemaphore(mainDevice.logicalDevice, imageAvailable.at(i), nullptr);
		vkDestroyFence(mainDevice.logicalDevice, drawFences.at(i), nullptr);
	}

	/*for(VkFramebuffer framebuffer : swapChainFramebuffers)
	{
		vkDestroyFramebuffer(mainDevice.logicalDevice, framebuffer, nullptr);
	}*/

	vkDestroyPipeline(mainDevice.logicalDevice, graphicsPipeline, nullptr);
	vkDestroyPipelineLayout(mainDevice.logicalDevice, pipelineLayout, nullptr);
	vkDestroyRenderPass(mainDevice.logicalDevice, renderPass, nullptr);
	//vkDestroyDescriptorSetLayout(mainDevice.logicalDevice, descriptorSetLayout, nullptr);

	/*for(SwapChainImage image : swapChainImages)
	{
		vkDestroyImageView(mainDevice.logicalDevice, image.imageView, nullptr);
	}

	vkDestroySwapchainKHR(mainDevice.logicalDevice, swapchain, nullptr);*/
	vkDestroySurfaceKHR(m_instance, surface, nullptr);
	vkDestroyDevice(mainDevice.logicalDevice, nullptr);

	if(validationEnabled)
	{
		DestroyDebugReportCallbackEXT(m_instance, callback, nullptr);
	}

	vkDestroyInstance(m_instance, nullptr);
}

VulkanRenderer::~VulkanRenderer()
{
}

void VulkanRenderer::recreateSwapChain()
{
	vkDeviceWaitIdle(mainDevice.logicalDevice);

	std::cout <<"\n\t---> [ VulkanRenderer::recreateSwapChain " << __LINE__ <<", " << __func__ <<" ] <---\n" << std::flush;

	cleanupSwapChain();

	createSwapChain();
	createRenderPass();
	createDescriptorSetLayout();
	//createGraphicsPipeline();
	createFramebuffers();
	createCommandPool();
	createCommandBuffers();
	createUniformBuffers();
	createDescriptorPool();
	createDescriptorSets();
	recordCommands();
}

void VulkanRenderer::cleanupSwapChain()
{
	vkFreeDescriptorSets(mainDevice.logicalDevice, descriptorPool, static_cast<uint32_t>(descriptorSets.size()), descriptorSets.data());

	vkDestroyDescriptorPool(mainDevice.logicalDevice, descriptorPool, nullptr);

	for(size_t i{ 0 }; i < swapChainImages.size(); ++i)
	{
		vkDestroyBuffer(mainDevice.logicalDevice, vpUniformBuffer.at(i), nullptr);
		vkFreeMemory(mainDevice.logicalDevice, vpUniformBufferMemory.at(i), nullptr);

		vkDestroyBuffer(mainDevice.logicalDevice, modelDUniformBuffer.at(i), nullptr);
		vkFreeMemory(mainDevice.logicalDevice, modelDUniformBufferMemory.at(i), nullptr);
	}

	for(VkFramebuffer framebuffer : swapChainFramebuffers)
	{
		vkDestroyFramebuffer(mainDevice.logicalDevice, framebuffer, nullptr);
	}

	//vkFreeCommandBuffers(mainDevice.logicalDevice, graphicsCommandPool, static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data());

	vkResetCommandPool(mainDevice.logicalDevice, graphicsCommandPool, VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT);
	
	for(VkCommandBuffer cmdBuffer : commandBuffers)
	{
		vkResetCommandBuffer(cmdBuffer, 0/*VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT*/);
	}

	/*vkDestroyPipeline(mainDevice.logicalDevice, graphicsPipeline, nullptr);
	vkDestroyPipelineLayout(mainDevice.logicalDevice, pipelineLayout, nullptr);
	vkDestroyRenderPass(mainDevice.logicalDevice, renderPass, nullptr);*/
	vkDestroyDescriptorSetLayout(mainDevice.logicalDevice, descriptorSetLayout, nullptr);

	for(SwapChainImage image : swapChainImages)
	{
		vkDestroyImageView(mainDevice.logicalDevice, image.imageView, nullptr);
	}

	vkDestroySwapchainKHR(mainDevice.logicalDevice, swapchain, nullptr);

	swapChainImages.clear();
	swapChainFramebuffers.clear();
}

void VulkanRenderer::createInstance()
{
	if(validationEnabled && !checkValidationLayerSupport())
	{
		throw std::runtime_error("Required validation Layers not supported: ");
	}

	//Information about the applications itself
	//Most data here doesn't affect the program itself, and is
	//for developers convenience
	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = "Vulkan App";
	appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.pEngineName = "No Engine";
	appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.apiVersion = VK_API_VERSION_1_2;		//this one is usefull for the program

	//Creation information for a vulkan instance
	VkInstanceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.flags = 0;
	createInfo.pApplicationInfo = &appInfo;

	//create list to hold instance extensions
	std::vector<const char*> instanceExtensions = std::vector<const char*>();

	//set up extensions instance will use
	uint32_t sdlExtensionsCount{ 0 };
	SDL_Vulkan_GetInstanceExtensions(m_window, &sdlExtensionsCount, nullptr);
	std::vector<const char*> sdlExtensionsNames(sdlExtensionsCount);
	SDL_Vulkan_GetInstanceExtensions(m_window, &sdlExtensionsCount, sdlExtensionsNames.data());

	//Add SDL extensions to list of extensions
	for(size_t i{ 0 }; i < sdlExtensionsCount; ++i)
	{
		instanceExtensions.push_back(sdlExtensionsNames.at(i));
	}

	if(validationEnabled)
	{
		std::cout <<"\n\t----> " << __FILE__ <<" validationEnabled " << __func__ <<":" << __LINE__ << " <----" << std::flush << std::endl;
		instanceExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
	}

	if(!checkInstanceExtensionSupport(&instanceExtensions))
	{
		throw std::runtime_error("vkInstance does not support required extensions");
	}

	createInfo.enabledExtensionCount = static_cast<uint32_t>(instanceExtensions.size());
	createInfo.ppEnabledExtensionNames = instanceExtensions.data();

	if(validationEnabled)
	{
		createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
		createInfo.ppEnabledLayerNames = validationLayers.data();
	}
	else
	{
		createInfo.enabledLayerCount = 0;
		createInfo.ppEnabledLayerNames = nullptr;
	}

	//Create Instance
	if(VkResult result = vkCreateInstance(&createInfo, nullptr, &m_instance); result != VK_SUCCESS)
	{
		std::string error = "Failed to create Vulkan Instance: ";
		error += result;
		throw std::runtime_error(error);
	}
}

void VulkanRenderer::createDebugCallback()
{
	if(!validationEnabled)
	{
		return;
	}

	VkDebugReportCallbackCreateInfoEXT callbackCreateInfo = {};
	callbackCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
	callbackCreateInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
	callbackCreateInfo.pfnCallback = debugCallback;

	if(VkResult result = CreateDebugReportCallbackEXT(m_instance, &callbackCreateInfo, nullptr, &callback); result != VK_SUCCESS)
	{
		throw std::runtime_error("Failed to create debug callback");
	}
}

void VulkanRenderer::createLogicalDevice()
{
	//get queue family indices for the chosen physical device
	QueueFamilyIndices indices = getQueueFamilies(mainDevice.physicalDevice);

	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
	std::set<int> queueFamilyIndices = { indices.graphicsFamily, indices.presentationFamily };

	for(int queueFamilyIndex : queueFamilyIndices)
	{
		//Queue the logical device needs to create and info to do so
		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = queueFamilyIndex;
		queueCreateInfo.queueCount = 1;
		float priority{ 1.0f };
		queueCreateInfo.pQueuePriorities = &priority;

		queueCreateInfos.push_back(queueCreateInfo);
	}

	//info to create logical device, device usually refers to logical device
	VkDeviceCreateInfo deviceCreateInfo = {};
	deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceCreateInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
	deviceCreateInfo.pQueueCreateInfos = queueCreateInfos.data();
	deviceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
	deviceCreateInfo.ppEnabledExtensionNames = deviceExtensions.data();

	//physical device features logical device will be using
	VkPhysicalDeviceFeatures deviceFeatures = {};

	deviceCreateInfo.pEnabledFeatures = &deviceFeatures;

	//create logical device
	if(VkResult result = vkCreateDevice(mainDevice.physicalDevice, &deviceCreateInfo, nullptr, &mainDevice.logicalDevice); result != VK_SUCCESS)
	{
		std::string error = "Failed to create Vulkan Logical Device: ";
		error += result;
		throw std::runtime_error(error);
	}

	//queues are created at the same time as the device
	vkGetDeviceQueue(mainDevice.logicalDevice, indices.graphicsFamily, 0, &graphicsQueue);
	vkGetDeviceQueue(mainDevice.logicalDevice, indices.presentationFamily, 0, &presentationQueue);
}

void VulkanRenderer::createSurface()
{
	if(SDL_bool result = SDL_Vulkan_CreateSurface(m_window, m_instance, &surface); result != SDL_TRUE)
	{
		std::string err = "SDL_Vulkan_CreateSurface: ";
		err += result;
		throw std::runtime_error(err);
	}
}

void VulkanRenderer::createSwapChain()
{
	SwapChainDetails swapChainDetails = getSwapChainDetails(mainDevice.physicalDevice);

	//1. choose best surface format
	//2. choose best presentation mode
	//3. choose swapchain image resolution
	VkSurfaceFormatKHR surfaceFormat = chooseBestSurfaceFormat(swapChainDetails.formats);
	VkPresentModeKHR presentMode = chooseBestPresentationMode(swapChainDetails.presentationModes);
	VkExtent2D extent = chooseSwapExtent(swapChainDetails.surfaceCapabilities);

	//how many images are in the swap chain?
	uint32_t imageCount = swapChainDetails.surfaceCapabilities.minImageCount + 1;

	if((swapChainDetails.surfaceCapabilities.maxImageCount > 0)
			&& (swapChainDetails.surfaceCapabilities.maxImageCount < imageCount))
	{
		imageCount = swapChainDetails.surfaceCapabilities.maxImageCount;
	}

	VkSwapchainCreateInfoKHR swapChainCreateInfo = {};
	swapChainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	swapChainCreateInfo.surface = surface;
	swapChainCreateInfo.imageFormat = surfaceFormat.format;
	swapChainCreateInfo.imageColorSpace = surfaceFormat.colorSpace;
	swapChainCreateInfo.presentMode = presentMode;
	swapChainCreateInfo.imageExtent = extent;
	swapChainCreateInfo.minImageCount = imageCount;
	swapChainCreateInfo.imageArrayLayers = 1;
	swapChainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	swapChainCreateInfo.preTransform = swapChainDetails.surfaceCapabilities.currentTransform;
	swapChainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	swapChainCreateInfo.clipped = VK_TRUE;

	//get queue family indices
	QueueFamilyIndices indices = getQueueFamilies(mainDevice.physicalDevice);
	if(indices.graphicsFamily != indices.presentationFamily)
	{
		uint32_t queueFamilyIndices[] =
		{
				(uint32_t)indices.graphicsFamily,
				(uint32_t)indices.presentationFamily
		};

		swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		swapChainCreateInfo.queueFamilyIndexCount = 2;
		swapChainCreateInfo.pQueueFamilyIndices = queueFamilyIndices;
	}
	else
	{
		swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		swapChainCreateInfo.queueFamilyIndexCount = 0;
		swapChainCreateInfo.pQueueFamilyIndices = nullptr;
	}

	swapChainCreateInfo.oldSwapchain = VK_NULL_HANDLE;

	//create swapchain
	if(VkResult res = vkCreateSwapchainKHR(mainDevice.logicalDevice, &swapChainCreateInfo, nullptr, &swapchain); res != VK_SUCCESS)
	{
		std::string err = "Failed to create swapchain: ";
		err += res;
		throw std::runtime_error(err);
	}

	swapChainImageFormat = surfaceFormat.format;
	swapChainExtent = extent;

	uint32_t swapChainImageCount{ 0 };
	vkGetSwapchainImagesKHR(mainDevice.logicalDevice, swapchain, &swapChainImageCount, nullptr);
	std::vector<VkImage> images(swapChainImageCount);
	vkGetSwapchainImagesKHR(mainDevice.logicalDevice, swapchain, &swapChainImageCount, images.data());

	for(VkImage image : images)
	{
		//store image handle
		SwapChainImage swapChainImage = {};
		swapChainImage.image = image;

		//Create image view here
		swapChainImage.imageView = createImageView(image, swapChainImageFormat, VK_IMAGE_ASPECT_COLOR_BIT);

		swapChainImages.push_back(swapChainImage);
	}
}

void VulkanRenderer::createRenderPass()
{
	//color attachment of renderpass
	VkAttachmentDescription colourAttachment = {};
	colourAttachment.format = swapChainImageFormat;
	colourAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
	colourAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;	//AKA glClear
	colourAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colourAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colourAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colourAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colourAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	VkAttachmentReference colourAttachmentReference = {};
	colourAttachmentReference.attachment = 0;
	colourAttachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subpass = {};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &colourAttachmentReference;

	//Need to determine when layout transitions occur using subpass dependencies
	std::array<VkSubpassDependency, 2> subpassDependencies;
	//conversion from VK_IMAGE_LAYOUT_UNDEFINED to VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
	//transition must happen after ...
	subpassDependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
	subpassDependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	subpassDependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
	//transition must happen befor ...
	subpassDependencies[0].dstSubpass = 0;
	subpassDependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	subpassDependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	subpassDependencies[0].dependencyFlags = 0;

	//conversion from VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL to VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
	//transition must happen after ...
	subpassDependencies[1].srcSubpass = 0;
	subpassDependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	subpassDependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	//transition must happen befor ...
	subpassDependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
	subpassDependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	subpassDependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
	subpassDependencies[1].dependencyFlags = 0;

	VkRenderPassCreateInfo renderPassCreateInfo = {};
	renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassCreateInfo.attachmentCount = 1;
	renderPassCreateInfo.pAttachments = &colourAttachment;
	renderPassCreateInfo.subpassCount = 1;
	renderPassCreateInfo.pSubpasses = &subpass;
	renderPassCreateInfo.dependencyCount = static_cast<uint32_t>(subpassDependencies.size());
	renderPassCreateInfo.pDependencies = subpassDependencies.data();

	if(VkResult res = vkCreateRenderPass(mainDevice.logicalDevice, &renderPassCreateInfo, nullptr, &renderPass); res != VK_SUCCESS)
	{
		throw std::runtime_error("Failed to create a render Pass: " + res);
	}
}

void VulkanRenderer::createDescriptorSetLayout()
{
	//UboViewProjection binding info
	VkDescriptorSetLayoutBinding mvpLayoutBinding = {};
	mvpLayoutBinding.binding = 0;
	mvpLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	mvpLayoutBinding.descriptorCount = 1;
	mvpLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
	mvpLayoutBinding.pImmutableSamplers = nullptr;

	//Model binding info
	VkDescriptorSetLayoutBinding modelLayoutBinding = {};
	modelLayoutBinding.binding = 1;
	modelLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
	modelLayoutBinding.descriptorCount = 1;
	modelLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
	modelLayoutBinding.pImmutableSamplers = nullptr;

	std::vector<VkDescriptorSetLayoutBinding> layoutBindings =
	{
		mvpLayoutBinding,
		modelLayoutBinding
	};
	
	//Create descriptor set layout with
	//given bindings
	VkDescriptorSetLayoutCreateInfo layoutCreateInfo = {};
	layoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutCreateInfo.bindingCount = static_cast<uint32_t>(layoutBindings.size());
	layoutCreateInfo.pBindings = layoutBindings.data();

	//Create descriptor set layout
	if(VkResult res = vkCreateDescriptorSetLayout(mainDevice.logicalDevice, &layoutCreateInfo, nullptr, &descriptorSetLayout); res != VK_SUCCESS)
	{
		throw std::runtime_error("Failed to create a Descriptor Set Layout: " + res);
	}
}

void VulkanRenderer::createGraphicsPipeline()
{
	//Read in SPIR-V code of shaders
	std::vector<char> vertexShaderCode = readFile("Shaders/vert.spv");
	std::vector<char> fragmentShaderCode = readFile("Shaders/frag.spv");

	//Build shader modules to link to graphics pipeline
	VkShaderModule vertexShaderModule = createShaderModule(vertexShaderCode);
	VkShaderModule fragmentShaderModule = createShaderModule(fragmentShaderCode);

	//-- Shader Stage Creation Information --
	//vertex stage creation information
	VkPipelineShaderStageCreateInfo vertexShaderCreateInfo = {};
	vertexShaderCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	vertexShaderCreateInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	vertexShaderCreateInfo.module = vertexShaderModule;
	vertexShaderCreateInfo.pName = "main";

	//Fragment stage creation information
	VkPipelineShaderStageCreateInfo fragmentShaderCreateInfo = {};
	fragmentShaderCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	fragmentShaderCreateInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fragmentShaderCreateInfo.module = fragmentShaderModule;
	fragmentShaderCreateInfo.pName = "main";

	//put shader stage creation info into array
	VkPipelineShaderStageCreateInfo shaderStages[] = { vertexShaderCreateInfo, fragmentShaderCreateInfo };

	//How the data for a single vertex (including info such as position, color, texture coords, normals, etc)
	//is as a whole.
	VkVertexInputBindingDescription bindingDescription = {};
	bindingDescription.binding = 0;
	bindingDescription.stride = sizeof(Vertex);
	bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	//How the data for an attribute is definded within a vertex
	std::array<VkVertexInputAttributeDescription, 2> attributeDescriptions;
	//Position attribute
	attributeDescriptions.at(0).binding = 0;
	attributeDescriptions.at(0).location = 0;
	attributeDescriptions.at(0).format = VK_FORMAT_R32G32B32_SFLOAT;
	attributeDescriptions.at(0).offset = offsetof(Vertex, pos);
	//Color attributes
	attributeDescriptions.at(1).binding = 0;
	attributeDescriptions.at(1).location = 1;
	attributeDescriptions.at(1).format = VK_FORMAT_R32G32B32_SFLOAT;
	attributeDescriptions.at(1).offset = offsetof(Vertex, col);

	//-- vertex input --
	VkPipelineVertexInputStateCreateInfo vertexInputCreateInfo = {};
	vertexInputCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputCreateInfo.vertexBindingDescriptionCount = 1;
	vertexInputCreateInfo.pVertexBindingDescriptions = &bindingDescription;
	vertexInputCreateInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
	vertexInputCreateInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

	// -- Input assembly --
	VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssembly.primitiveRestartEnable = VK_FALSE;

	// -- Viewport and scissor --
	//create a viewport info struct
	VkViewport viewport = {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = static_cast<float>(swapChainExtent.width);
	viewport.height = static_cast<float>(swapChainExtent.height);
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	//create a scissor info struct
	VkRect2D scissor = {};
	scissor.offset = { 0, 0 };
	scissor.extent = swapChainExtent;

	VkPipelineViewportStateCreateInfo viewportStateCreateInfo = {};
	viewportStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportStateCreateInfo.viewportCount = 1;
	viewportStateCreateInfo.pViewports = nullptr;	//&viewport //Dynamic States
	viewportStateCreateInfo.scissorCount = 1;
	viewportStateCreateInfo.pScissors = nullptr;	//&scissor;	//Dynamic States

	//-- DYNAMIC STATES --
	//Dynamic states to enable
	std::vector<VkDynamicState> dynamicStatesEnables;
	dynamicStatesEnables.push_back(VK_DYNAMIC_STATE_VIEWPORT);	//Dynamic viewport : resize in command buffer
	dynamicStatesEnables.push_back(VK_DYNAMIC_STATE_SCISSOR);
	//Dynamic state creation info
	VkPipelineDynamicStateCreateInfo dynamicStateCreateInfo = {};
	dynamicStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamicStateCreateInfo.dynamicStateCount = static_cast<uint32_t>(dynamicStatesEnables.size());
	dynamicStateCreateInfo.pDynamicStates = dynamicStatesEnables.data();
	//-- DYNAMIC STATES --

	// -- Rasterizer --
	VkPipelineRasterizationStateCreateInfo rasterizerCreateInfo = {};
	rasterizerCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizerCreateInfo.depthClampEnable = VK_FALSE;
	rasterizerCreateInfo.rasterizerDiscardEnable = VK_FALSE;
	rasterizerCreateInfo.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizerCreateInfo.lineWidth = 1.0f;
	rasterizerCreateInfo.cullMode = VK_CULL_MODE_BACK_BIT;
	rasterizerCreateInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	rasterizerCreateInfo.depthBiasEnable = VK_FALSE;

	//-- Multisampling --
	VkPipelineMultisampleStateCreateInfo multisamplingCreateInfo = {};
	multisamplingCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisamplingCreateInfo.sampleShadingEnable = VK_FALSE;
	multisamplingCreateInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

	//-- Blending --
	//blend attachment state
	VkPipelineColorBlendAttachmentState colourState = {};
	colourState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colourState.blendEnable = VK_TRUE;
	colourState.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
	colourState.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	colourState.colorBlendOp = VK_BLEND_OP_ADD;
	colourState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	colourState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	colourState.alphaBlendOp = VK_BLEND_OP_ADD;

	VkPipelineColorBlendStateCreateInfo colourBlendingCreateInfo = {};
	colourBlendingCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colourBlendingCreateInfo.logicOpEnable = VK_FALSE;
	colourBlendingCreateInfo.attachmentCount = 1;
	colourBlendingCreateInfo.pAttachments = &colourState;

	//-- Pipeline Layout --
	VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {};
	pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutCreateInfo.setLayoutCount = 1;
	pipelineLayoutCreateInfo.pSetLayouts = &descriptorSetLayout;
	pipelineLayoutCreateInfo.pushConstantRangeCount = 0;
	pipelineLayoutCreateInfo.pPushConstantRanges = nullptr;

	//create pipeline layout
	if(VkResult result = vkCreatePipelineLayout(mainDevice.logicalDevice, &pipelineLayoutCreateInfo, nullptr, &pipelineLayout); result != VK_SUCCESS)
	{
		throw std::runtime_error("Failed to create pipeline Layout: " + result);
	}

	// -- Depth stencil testing --
	//TODO set up depth stencil testing

	//-- Graphics pipeline creation --
	VkGraphicsPipelineCreateInfo pipelineCreateInfo = {};
	pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineCreateInfo.stageCount = 2;
	pipelineCreateInfo.pStages = shaderStages;
	pipelineCreateInfo.pVertexInputState = &vertexInputCreateInfo;
	pipelineCreateInfo.pInputAssemblyState = &inputAssembly;
	pipelineCreateInfo.pViewportState = &viewportStateCreateInfo;
	pipelineCreateInfo.pDynamicState = &dynamicStateCreateInfo;										// < -- Dynamic States
	pipelineCreateInfo.pRasterizationState = &rasterizerCreateInfo;
	pipelineCreateInfo.pMultisampleState = &multisamplingCreateInfo;
	pipelineCreateInfo.pColorBlendState = &colourBlendingCreateInfo;
	pipelineCreateInfo.pDepthStencilState = nullptr;
	pipelineCreateInfo.layout = pipelineLayout;
	pipelineCreateInfo.renderPass = renderPass;
	pipelineCreateInfo.subpass = 0;
	pipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
	pipelineCreateInfo.basePipelineIndex = -1;

	if(VkResult res = vkCreateGraphicsPipelines(mainDevice.logicalDevice, VK_NULL_HANDLE, 1, &pipelineCreateInfo, nullptr, &graphicsPipeline); res != VK_SUCCESS)
	{
		throw std::runtime_error("Failed to create the graphics pipeline: " + res);

	}

	//destroy shader modules no longer needed
	vkDestroyShaderModule(mainDevice.logicalDevice, fragmentShaderModule, nullptr);
	vkDestroyShaderModule(mainDevice.logicalDevice, vertexShaderModule, nullptr);
}

void VulkanRenderer::createFramebuffers()
{
	swapChainFramebuffers.resize(swapChainImages.size());

	for(size_t i{ 0 }; i < swapChainFramebuffers.size(); ++i)
	{
		std::array<VkImageView, 1> attachments =
		{
			swapChainImages[i].imageView
		};

		VkFramebufferCreateInfo framebufferCreateInfo = {};
		framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferCreateInfo.renderPass = renderPass;
		framebufferCreateInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
		framebufferCreateInfo.pAttachments = attachments.data();
		framebufferCreateInfo.width = swapChainExtent.width;
		framebufferCreateInfo.height = swapChainExtent.height;
		framebufferCreateInfo.layers = 1;

		if(VkResult res = vkCreateFramebuffer(mainDevice.logicalDevice, &framebufferCreateInfo, nullptr, &swapChainFramebuffers.at(i)); res != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to create a framebuffer: " + res);
		}
	}
}

void VulkanRenderer::createCommandPool()
{
	QueueFamilyIndices queueFamilyIndices = getQueueFamilies(mainDevice.physicalDevice);

	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily;
	poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

	//Create a Graphics Queue Family Command Pool
	if(VkResult res = vkCreateCommandPool(mainDevice.logicalDevice, &poolInfo, nullptr, &graphicsCommandPool); res != VK_SUCCESS)
	{
		throw std::runtime_error("Failed to create a command pool: " + res);
	}
}

void VulkanRenderer::createCommandBuffers()
{
	commandBuffers.resize(swapChainFramebuffers.size());
	VkCommandBufferAllocateInfo cbAllocInfo = {};
	cbAllocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	cbAllocInfo.commandPool = graphicsCommandPool;
	cbAllocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	cbAllocInfo.commandBufferCount = static_cast<uint32_t>(commandBuffers.size());

	if(VkResult res = vkAllocateCommandBuffers(mainDevice.logicalDevice, &cbAllocInfo, commandBuffers.data()); res != VK_SUCCESS)
	{
		throw std::runtime_error("Failed to allocate Command Buffers: " + res);
	}
}

void VulkanRenderer::createSynchronisation()
{
	imageAvailable.resize(MAX_FRAME_DRAWS);
	renderFinished.resize(MAX_FRAME_DRAWS);
	drawFences.resize(MAX_FRAME_DRAWS);

	//Semaphore creation information
	VkSemaphoreCreateInfo semaphoreCreateInfo = {};
	semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkFenceCreateInfo fenceCreateInfo = {};
	fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	for(size_t i{ 0 }; i < MAX_FRAME_DRAWS; ++i)
	{
		if(VkResult r1 = vkCreateSemaphore(mainDevice.logicalDevice, &semaphoreCreateInfo, nullptr, &imageAvailable.at(i)),
					r2 = vkCreateSemaphore(mainDevice.logicalDevice, &semaphoreCreateInfo, nullptr, &renderFinished.at(i)),
					r3 = vkCreateFence(mainDevice.logicalDevice, &fenceCreateInfo, nullptr, &drawFences.at(i));
		(r1 != VK_SUCCESS) || (r2 != VK_SUCCESS) || (r3 != VK_SUCCESS))
		{
			std::string err = "Failed to create semaphore: ";
			err += r1;
			err += ", ";
			err += r2;
			err += ", Fence: ";
			err += r3;
			throw std::runtime_error(err);
		}
	}
}

void VulkanRenderer::createUniformBuffers()
{
	VkDeviceSize vpBufferSize = sizeof(UboViewProjection);
	VkDeviceSize modleBufferSize = modelUniformAlignment * MAX_OBJECTS;

	//One uniform buffer for each image (and by extension, command buffer)
	vpUniformBuffer.resize(swapChainImages.size());
	vpUniformBufferMemory.resize(swapChainImages.size());

	modelDUniformBuffer.resize(swapChainImages.size());
	modelDUniformBufferMemory.resize(swapChainImages.size());

	//Create the uniform buffers
	for(size_t i{ 0 }; i < swapChainImages.size(); ++i)
	{
		createBuffer(mainDevice.physicalDevice, mainDevice.logicalDevice, vpBufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &vpUniformBuffer.at(i), &vpUniformBufferMemory.at(i));

		createBuffer(mainDevice.physicalDevice, mainDevice.logicalDevice, modleBufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &modelDUniformBuffer.at(i), &modelDUniformBufferMemory.at(i));
	}
}

void VulkanRenderer::createDescriptorPool()
{
	VkDescriptorPoolSize poolSize = {};
	poolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSize.descriptorCount = static_cast<uint32_t>(vpUniformBuffer.size());

	VkDescriptorPoolSize modelPoolSize = {};
	modelPoolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
	modelPoolSize.descriptorCount = static_cast<uint32_t>(modelDUniformBuffer.size());

	std::vector<VkDescriptorPoolSize> descriptorPoolSizes =
	{
		poolSize,
		modelPoolSize
	};

	VkDescriptorPoolCreateInfo poolCreateInfo = {};
	poolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolCreateInfo.maxSets = static_cast<uint32_t>(swapChainImages.size());
	poolCreateInfo.poolSizeCount = static_cast<uint32_t>(descriptorPoolSizes.size());
	poolCreateInfo.pPoolSizes = descriptorPoolSizes.data();
	poolCreateInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;

	if(VkResult r = vkCreateDescriptorPool(mainDevice.logicalDevice, &poolCreateInfo, nullptr, &descriptorPool); r != VK_SUCCESS)
	{
		throw std::runtime_error("Failed to create a Descriptor Pool: " + r);
	}
}

void VulkanRenderer::createDescriptorSets()
{
	descriptorSets.resize(swapChainImages.size());

	std::vector<VkDescriptorSetLayout> setLayouts(swapChainImages.size(), descriptorSetLayout);

	VkDescriptorSetAllocateInfo setAllocInfo = {};
	setAllocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	setAllocInfo.descriptorPool = descriptorPool;
	setAllocInfo.descriptorSetCount = static_cast<uint32_t>(swapChainImages.size());
	setAllocInfo.pSetLayouts = setLayouts.data();

	if(VkResult r = vkAllocateDescriptorSets(mainDevice.logicalDevice, &setAllocInfo, descriptorSets.data()); r != VK_SUCCESS)
	{
		throw std::runtime_error("Failed to allocate Descriptor Sets: " + r);
	}

	for(size_t i{ 0 }; i < swapChainImages.size(); ++i)
	{
		//View Projection Descriptor
		VkDescriptorBufferInfo mvpBufferInfo = {};
		mvpBufferInfo.buffer = vpUniformBuffer.at(i);
		mvpBufferInfo.offset = 0;
		mvpBufferInfo.range = sizeof(UboViewProjection);

		VkWriteDescriptorSet mvpSetWrite = {};
		mvpSetWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		mvpSetWrite.dstSet = descriptorSets.at(i);
		mvpSetWrite.dstBinding = 0;
		mvpSetWrite.dstArrayElement = 0;
		mvpSetWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		mvpSetWrite.descriptorCount = 1;
		mvpSetWrite.pBufferInfo = &mvpBufferInfo;
		mvpSetWrite.pImageInfo = nullptr;
		mvpSetWrite.pTexelBufferView = nullptr;

		//Model Descriptor
		VkDescriptorBufferInfo modelBufferInfo = {};
		modelBufferInfo.buffer = modelDUniformBuffer.at(i);
		modelBufferInfo.offset = 0;
		modelBufferInfo.range = modelUniformAlignment;

		VkWriteDescriptorSet modelSetWrite = {};
		modelSetWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		modelSetWrite.dstSet = descriptorSets.at(i);
		modelSetWrite.dstBinding = 1;
		modelSetWrite.dstArrayElement = 0;
		modelSetWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
		modelSetWrite.descriptorCount = 1;
		modelSetWrite.pBufferInfo = &modelBufferInfo;
		modelSetWrite.pImageInfo = nullptr;
		modelSetWrite.pTexelBufferView = nullptr;

		std::vector<VkWriteDescriptorSet> setWrites =
		{
			mvpSetWrite,
			modelSetWrite
		};

		vkUpdateDescriptorSets(mainDevice.logicalDevice, static_cast<uint32_t>(setWrites.size()), setWrites.data(), 0, nullptr);
	}
}

void VulkanRenderer::updateUniformBufers(uint32_t imageIndex)
{
	//Copy view projection (vp) data
	void *data{ nullptr };
	vkMapMemory(mainDevice.logicalDevice, vpUniformBufferMemory.at(imageIndex), 0, sizeof(UboViewProjection), 0, &data);
	memcpy(data, &uboViewProjection, sizeof(UboViewProjection));
	vkUnmapMemory(mainDevice.logicalDevice, vpUniformBufferMemory.at(imageIndex));
	data = nullptr;

	//Copy model data
	for(size_t i{ 0 }; i < meshList.size(); ++i)
	{
		UboModel *thisModel = (UboModel*)((uint64_t)modelTransferSpace + (i * modelUniformAlignment));
		*thisModel = meshList.at(i).getModel();
	}

	//Map the list of model data
	vkMapMemory(mainDevice.logicalDevice, modelDUniformBufferMemory.at(imageIndex), 0, modelUniformAlignment * meshList.size(), 0, &data);
	memcpy(data, modelTransferSpace, modelUniformAlignment * meshList.size());
	vkUnmapMemory(mainDevice.logicalDevice, modelDUniformBufferMemory.at(imageIndex));
	data = nullptr;
}

void VulkanRenderer::recordCommands()
{
	//Information about how to begin each command buffer
	VkCommandBufferBeginInfo bufferBeginInfo = {};
	bufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	//bufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

	//Information about how to begin a render pass
	VkRenderPassBeginInfo renderPassBeginInfo = {};
	renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassBeginInfo.renderPass = renderPass;
	renderPassBeginInfo.renderArea.offset = { 0, 0 };
	renderPassBeginInfo.renderArea.extent = swapChainExtent;

	//AKA glClearColor(r, g, b, A)
	VkClearValue clearValues[] =
	{
		{ 0.33f, 0.33f, 0.33f, 1.0f }
	};

	renderPassBeginInfo.pClearValues = clearValues;	//list of clear values TODO depth attachment clear value
	renderPassBeginInfo.clearValueCount = 1;

	for(size_t i{ 0 }; i < commandBuffers.size(); ++i)
	{
		renderPassBeginInfo.framebuffer = swapChainFramebuffers.at(i);

		//start recording commands to command buffer
		if(VkResult res = vkBeginCommandBuffer(commandBuffers.at(i), &bufferBeginInfo); res != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to start recording a command buffer: " + res);
		}

		//start renderpass
		vkCmdBeginRenderPass(commandBuffers.at(i), &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

		// <-- Dynamic States -->
		// -- Viewport and scissor --
		//create a viewport info struct
		VkViewport viewport = {};
		viewport.x = 0.0f;
		viewport.y = 0.0f;
		viewport.width = static_cast<float>(swapChainExtent.width);
		viewport.height = static_cast<float>(swapChainExtent.height);
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;
		vkCmdSetViewport(commandBuffers.at(i), 0, 1, &viewport);

		//create a scissor info struct
		VkRect2D scissor = {};
		scissor.offset = { 0, 0 };
		scissor.extent = swapChainExtent;
		vkCmdSetScissor(commandBuffers.at(i), 0, 1, &scissor);
		// <-- Dynamic States -->

		//Bind pipeline to be used in render pass
		vkCmdBindPipeline(commandBuffers.at(i), VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

		for(size_t j{ 0 }; j < meshList.size(); ++j)
		{
			//Buffers to bind
			VkBuffer vertexBuffers[] = { meshList.at(j).getVertexBuffer() };

			//Offsets into buffers being bound
			VkDeviceSize offsets[] = { 0 };

			//command to bind vertex buffer before drawing with them
			vkCmdBindVertexBuffers(commandBuffers.at(i), 0, 1, vertexBuffers, offsets);

			//Bind mesh index buffer
			vkCmdBindIndexBuffer(commandBuffers.at(i), meshList.at(j).getIndexBuffer(), 0, VK_INDEX_TYPE_UINT32);

			//Dynamic Offset Amaount
			uint32_t dynamicOffset{ static_cast<uint32_t>(modelUniformAlignment) * j };

			//Bind Descriptor Sets
			vkCmdBindDescriptorSets(commandBuffers.at(i), VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descriptorSets.at(i), 1, &dynamicOffset);

			//Execute pipeline
			//vkCmdDraw(commandBuffers.at(i), firstMesh.getVertexCount(), 1, 0, 0);
			vkCmdDrawIndexed(commandBuffers.at(i), meshList.at(j).getIndexCount(), 1, 0, 0, 0);
		}

		//end renderpass
		vkCmdEndRenderPass(commandBuffers.at(i));

		//stop recording commands
		if(VkResult res = vkEndCommandBuffer(commandBuffers.at(i)); res != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to stop recording a command buffer: " + res);
		}
	}
}

void VulkanRenderer::getPhysicalDevice()
{
	//Enumerate physical devices de vkInstance can access
	uint32_t deviceCount{ 0 };
	vkEnumeratePhysicalDevices(m_instance, &deviceCount, nullptr);

	//if no devices available, then none support Vulkan
	if(deviceCount == 0)
	{
		throw std::runtime_error("Can't find GPUs that support Vulkan Instance");
	}

	std::vector<VkPhysicalDevice> deviceList(deviceCount);
	vkEnumeratePhysicalDevices(m_instance, &deviceCount, deviceList.data());

	for(const VkPhysicalDevice &device : deviceList)
	{
		if(checkDeviceSuitable(device))
		{
			mainDevice.physicalDevice = device;
			break;
		}
	}

	//Get properties of our new device
	VkPhysicalDeviceProperties deviceProperties;
	vkGetPhysicalDeviceProperties(mainDevice.physicalDevice, &deviceProperties);
	minUniformBufferOffset = deviceProperties.limits.minUniformBufferOffsetAlignment;
}

void VulkanRenderer::allocateDynamicBufferTransferSpace()
{
	//Calculate alignment of model data
	modelUniformAlignment = (sizeof(UboModel) + minUniformBufferOffset - 1) & ~(minUniformBufferOffset - 1);

	//Create space in memoryto hold dynamic buffer that is aligned to our required alignment and holds MAX_OBJECTS
	modelTransferSpace = (UboModel*)aligned_alloc(modelUniformAlignment, modelUniformAlignment * MAX_OBJECTS);
}

bool VulkanRenderer::checkInstanceExtensionSupport(std::vector<const char*> *checkExtensions)
{
	//first get the number of extensions to create array of the correct size
	uint32_t extensionCount{ 0 };
	vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);

	//create a list of vkExtensionProperties using extensionCount
	std::vector<VkExtensionProperties> extensions(extensionCount);
	vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensions.data());

	//check if given extensions are in list of available extensions
	for(const char* const &checkExtension : *checkExtensions)
	{
		bool hasExtension{ false };

		for(const VkExtensionProperties &extension : extensions)
		{
			if(strcmp(checkExtension, extension.extensionName) == 0)
			{
				hasExtension = true;
				break;
			}
		}

		if(!hasExtension)
		{
			return false;
		}
	}

	return true;
}

bool VulkanRenderer::checkDeviceExtensionSupport(VkPhysicalDevice device)
{
	uint32_t extensionCount{ 0 };
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

	if(extensionCount == 0)
	{
		return false;
	}

	std::vector<VkExtensionProperties> extensions(extensionCount);
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, extensions.data());

	for(const char* const &deviceExtension : deviceExtensions)
	{
		bool hasExtension{ false };

		for(const VkExtensionProperties &extension : extensions)
		{
			if(strcmp(deviceExtension, extension.extensionName) == 0)
			{
				hasExtension = true;
				break;
			}
		}

		if(!hasExtension)
		{
			return false;
		}
	}

	return true;
}

bool VulkanRenderer::checkValidationLayerSupport()
{
	//get number of validation layers
	uint32_t validationLayerCount{ 0 };
	vkEnumerateInstanceLayerProperties(&validationLayerCount, nullptr);

	if((validationLayerCount == 0) /*&& (validationLayers.size() > 0)*/)
	{
		return false;
	}

	std::vector<VkLayerProperties> availableLayers(validationLayerCount);
	vkEnumerateInstanceLayerProperties(&validationLayerCount, availableLayers.data());

	for(const char* const &validationLayer : validationLayers)
	{
		bool hasLayer{ false };

		for(const VkLayerProperties &availableLayer : availableLayers)
		{
			if(strcmp(validationLayer, availableLayer.layerName) == 0)
			{
				hasLayer = true;
				break;
			}
		}

		if(!hasLayer)
		{
			return false;
		}
	}

	return true;
}

bool VulkanRenderer::checkDeviceSuitable(VkPhysicalDevice device)
{
	/*
	//information about the device itself
	VkPhysicalDeviceProperties deviceProperties;
	vkGetPhysicalDeviceProperties(device, &deviceProperties);

	//information about what the device can do
	VkPhysicalDeviceFeatures deviceFeatures;
	vkGetPhysicalDeviceFeatures(device, &deviceFeatures);
	*/

	QueueFamilyIndices indices = getQueueFamilies(device);

	bool extensionsSupported = checkDeviceExtensionSupport(device);

	bool swapChainValid{ false };

	if(extensionsSupported)
	{
		SwapChainDetails swapChainDetails = getSwapChainDetails(device);

		swapChainValid = !swapChainDetails.presentationModes.empty() && !swapChainDetails.formats.empty();
	}

	return indices.isValid() && extensionsSupported && swapChainValid;
}

QueueFamilyIndices VulkanRenderer::getQueueFamilies(VkPhysicalDevice device)
{
	QueueFamilyIndices indices;

	uint32_t queueFamilyCount{ 0 };
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

	std::vector<VkQueueFamilyProperties> queueFamilyList(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilyList.data());

	//go through each queue family and check if it has at least one required type of queue
	int i{ 0 };
	for(const VkQueueFamilyProperties &queueFamily : queueFamilyList)
	{
		//first check if queue family has at least 1 queue in that family
		//queue can be multiple types defined through bittfield
		if((queueFamily.queueCount > 0) && (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT))
		{
			indices.graphicsFamily = i;
		}

		VkBool32 presentationSupport{ false };
		vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentationSupport);

		if(queueFamily.queueCount > 0 && presentationSupport)
		{
			indices.presentationFamily = i;
		}

		//check if queue family indices are in a valid state
		if(indices.isValid())
		{
			break;
		}

		++i;
	}

	return indices;
}

SwapChainDetails VulkanRenderer::getSwapChainDetails(VkPhysicalDevice device)
{
	SwapChainDetails swapChainDetails;

	// -- Capabilities --
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &swapChainDetails.surfaceCapabilities);

	//-- Formats --
	uint32_t formatCount{ 0 };
	vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);

	if(formatCount != 0)
	{
		swapChainDetails.formats.resize(formatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, swapChainDetails.formats.data());
	}

	//-- Presentation modes --
	uint32_t presentationCount{ 0 };
	vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentationCount, nullptr);

	if(presentationCount != 0)
	{
		swapChainDetails.presentationModes.resize(presentationCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentationCount, swapChainDetails.presentationModes.data());
	}

	return swapChainDetails;
}

VkSurfaceFormatKHR VulkanRenderer::chooseBestSurfaceFormat(const std::vector<VkSurfaceFormatKHR> &formats)
{
	//VK_FORMAT_R8G8B8A8_UNORM
	//VK_COLOR_SPACE_SRGB_NONLINEAR_KHR

	if((formats.size() == 1) && (formats.at(0).format == VK_FORMAT_UNDEFINED))
	{
		return { VK_FORMAT_R8G8B8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
	}

	for(const VkSurfaceFormatKHR &format : formats)
	{
		if(((format.format == VK_FORMAT_R8G8B8A8_UNORM) || (format.format == VK_FORMAT_B8G8R8_UNORM))
				&& format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
		{
			return format;
		}
	}

	return formats.at(0);
}

VkPresentModeKHR VulkanRenderer::chooseBestPresentationMode(const std::vector<VkPresentModeKHR> presentationModes)
{
	for(const VkPresentModeKHR &presentationMode : presentationModes)
	{
		if(presentationMode == VK_PRESENT_MODE_MAILBOX_KHR)
		{
			return presentationMode;
		}
	}

	return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D VulkanRenderer::chooseSwapExtent(const VkSurfaceCapabilitiesKHR &surfaceCapabilities)
{
	if((surfaceCapabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) && (!windowResized))
	{
		return surfaceCapabilities.currentExtent;
	}
	else if(windowResized)
	{
		int width{ 0 };
		int height{ 0 };
		
		SDL_Vulkan_GetDrawableSize(m_window, &width, &height);

		VkExtent2D newExtent2 = {};
		newExtent2.width = static_cast<uint32_t>(width);
		newExtent2.height = static_cast<uint32_t>(height);

		std::cout <<"\n\tSDL_Vulkan_GetDrawableSize[" << __FILE__ << ":" << __LINE__ << "]:" << width <<" x " << height << std::flush;

		//surface also defines max and min, so make sure within boundaries by clamping value
		newExtent2.width = std::max(surfaceCapabilities.minImageExtent.width, std::min(surfaceCapabilities.maxImageExtent.width, newExtent2.width));
		newExtent2.height = std::max(surfaceCapabilities.minImageExtent.height, std::min(surfaceCapabilities.maxImageExtent.height, newExtent2.height));

		newExtent.width = newExtent2.width;
		newExtent.height = newExtent2.height;

		return newExtent2;
	}
}

VkImageView VulkanRenderer::createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags)
{
	VkImageViewCreateInfo viewCreateInfo = {};
	viewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewCreateInfo.image = image;
	viewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	viewCreateInfo.format = format;
	viewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
	viewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
	viewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
	viewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

	//subresources allow the view to view only a part of an image
	viewCreateInfo.subresourceRange.aspectMask = aspectFlags;
	viewCreateInfo.subresourceRange.baseMipLevel = 0;
	viewCreateInfo.subresourceRange.levelCount = 1;
	viewCreateInfo.subresourceRange.baseArrayLayer = 0;
	viewCreateInfo.subresourceRange.layerCount = 1;

	VkImageView imageView;
	if(VkResult res = vkCreateImageView(mainDevice.logicalDevice, &viewCreateInfo, nullptr, &imageView); res != VK_SUCCESS)
	{
		std::string err = "Failed to create an image view: ";
		err += res;
		throw std::runtime_error(err);
	}

	return imageView;
}

VkShaderModule VulkanRenderer::createShaderModule(const std::vector<char> &code)
{
	VkShaderModuleCreateInfo shaderModuleCreateInfo = {};
	shaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	shaderModuleCreateInfo.codeSize = code.size();
	shaderModuleCreateInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());

	VkShaderModule shaderModule;

	if(VkResult result = vkCreateShaderModule(mainDevice.logicalDevice, &shaderModuleCreateInfo, nullptr, &shaderModule); result != VK_SUCCESS)
	{
		throw std::runtime_error("Failed to create a shader module: " + result);
	}

	return shaderModule;
}

#elif
#error Platform not supported!
#endif /* __linux__ */