#if __linux__ || __linux || __gnu_linux__
#ifndef VULKANVALIDATION_H_
#define VULKANVALIDATION_H_

#include <vulkan/vulkan.h>
#include <vector>
#include <iostream>

//#define DBUG

#ifdef DBUG
const bool validationEnabled{ true };
#else
const bool validationEnabled{ false };
#endif

const std::vector<const char*> validationLayers =
	{
		//"VK_LAYER_LUNARG_standard_validation"
		"VK_LAYER_KHRONOS_validation"
	};

//callback funtion for validation debugging
static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
		VkDebugReportFlagsEXT flags,
		VkDebugReportObjectTypeEXT objType,
		uint64_t obj,
		size_t location,
		int32_t code,
		const char *layerPrefix,
		const char *message,
		void *userData)
{
	if(flags & VK_DEBUG_REPORT_ERROR_BIT_EXT)
	{
		std::cerr << "\n\tValidation ERROR: " << message << std::endl;
		return VK_TRUE;
	}

	if(flags & VK_DEBUG_REPORT_WARNING_BIT_EXT)
	{
		std::cerr <<"\n\tValidation WARNING: " << message << std::endl;
		return VK_FALSE;
	}

	return VK_FALSE;
}

static VkResult CreateDebugReportCallbackEXT(
		VkInstance instance,
		const VkDebugReportCallbackCreateInfoEXT* pCreateInfo,
		const VkAllocationCallbacks* pAllocator,
		VkDebugReportCallbackEXT* pCallback)
{
	auto func = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
	//PFN_vkVoidFunction
	//PFN_vkCreateDebugReportCallbackEXT

	if(func != nullptr)
	{
		return func(instance, pCreateInfo, pAllocator, pCallback);
	}
	else
	{
		return VK_ERROR_EXTENSION_NOT_PRESENT;
	}
}

static void DestroyDebugReportCallbackEXT(VkInstance instance, VkDebugReportCallbackEXT callback, const VkAllocationCallbacks* pAllocator)
{
	auto func = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");

	if(func != nullptr)
	{
		func(instance, callback, pAllocator);
	}
}

#endif /* VULKANVALIDATION_H_ */
#elif
#error Platform not supported!
#endif /* __linux__ */