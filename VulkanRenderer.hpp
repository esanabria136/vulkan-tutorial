#if __linux__ || __linux || __gnu_linux__
#ifndef VULKANRENDERER_H_
#define VULKANRENDERER_H_

#include <SDL2/SDL.h>
#include <vulkan/vulkan.h>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <vector>
#include <set>
#include <algorithm>
#include <array>
#include <chrono>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Utilities.hpp"
#include "Mesh.hpp"

class VulkanRenderer
{
private:
	SDL_Window *m_window = nullptr;

	bool windowResized{ false };
	
	//FPS stats:
    std::chrono::system_clock::time_point m_start /*= std::chrono::system_clock::now()*/;
    std::chrono::system_clock::time_point m_end /*= std::chrono::system_clock::now()*/;

	//Window resized extent
	VkExtent2D newExtent{ 0, 0 };

public:
	VulkanRenderer();

	int init(SDL_Window *w);
	void resizeWindow(int w, int h);

	void updateModel(size_t modelID, glm::mat4 newModel);
	void draw();
	void cleanup();

	~VulkanRenderer();

private:
	size_t m_width;
	size_t m_height;

	size_t currentFrame{ 0 };

	//Scene objects
	std::vector<Mesh> meshList;

	//Scene settings
	//Model View Projection
	struct UboViewProjection
	{
		glm::mat4 projection;
		glm::mat4 view;
	} uboViewProjection;

	//Vulkan Components
	VkInstance m_instance;
	VkDebugReportCallbackEXT callback;

	struct
	{
		VkPhysicalDevice physicalDevice;
		VkDevice logicalDevice;
	} mainDevice;

	VkQueue graphicsQueue;
	VkQueue presentationQueue;
	VkSurfaceKHR surface;
	VkSwapchainKHR swapchain;
	std::vector<SwapChainImage> swapChainImages;
	std::vector<VkFramebuffer> swapChainFramebuffers;
	std::vector<VkCommandBuffer> commandBuffers;

	// - Descriptors
	VkDescriptorSetLayout descriptorSetLayout;

	VkDescriptorPool descriptorPool;
	std::vector<VkDescriptorSet> descriptorSets;

	std::vector<VkBuffer> vpUniformBuffer;
	std::vector<VkDeviceMemory> vpUniformBufferMemory;

	std::vector<VkBuffer> modelDUniformBuffer;
	std::vector<VkDeviceMemory> modelDUniformBufferMemory;

	VkDeviceSize minUniformBufferOffset;
	size_t modelUniformAlignment;
	UboModel *modelTransferSpace;

	// - Pipeline
	VkPipeline graphicsPipeline;
	VkPipelineLayout pipelineLayout;
	VkRenderPass renderPass;

	// - Pools
	VkCommandPool graphicsCommandPool;

	// - Utility
	VkFormat swapChainImageFormat;
	VkExtent2D swapChainExtent;

	// - Synchronisation
	std::vector<VkSemaphore> imageAvailable;
	std::vector<VkSemaphore> renderFinished;
	std::vector<VkFence> drawFences;

	// -- Recreate Swapchain
	void recreateSwapChain();
	void cleanupSwapChain();

	// -- Vulkan Create Functions --
	void createInstance();
	void createDebugCallback();
	void createLogicalDevice();
	void createSurface();
	void createSwapChain();
	void createRenderPass();
	void createDescriptorSetLayout();
	void createGraphicsPipeline();
	void createFramebuffers();
	void createCommandPool();
	void createCommandBuffers();
	void createSynchronisation();

	void createUniformBuffers();
	void createDescriptorPool();
	void createDescriptorSets();

	void updateUniformBufers(uint32_t imageIndex);

	// - Record functions
	void recordCommands();

	// - Get functions
	void getPhysicalDevice();

	// - Allocate functions
	void allocateDynamicBufferTransferSpace();

	// - support functins
	// -- checker funtions
	bool checkInstanceExtensionSupport(std::vector<const char*> *checkExtensions);
	bool checkDeviceExtensionSupport(VkPhysicalDevice device);
	bool checkValidationLayerSupport();
	bool checkDeviceSuitable(VkPhysicalDevice device);

	// -- getter functions
	QueueFamilyIndices getQueueFamilies(VkPhysicalDevice device);
	SwapChainDetails getSwapChainDetails(VkPhysicalDevice device);

	// -- choose funtions
	VkSurfaceFormatKHR chooseBestSurfaceFormat(const std::vector<VkSurfaceFormatKHR> &formats);
	VkPresentModeKHR chooseBestPresentationMode(const std::vector<VkPresentModeKHR> presentationModes);
	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR &surfaceCapabilities);

	// -- create functions
	VkImageView createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags);
	VkShaderModule createShaderModule(const std::vector<char> &code);
};

#endif /* VULKANRENDERER_H_ */
#elif
#error Platform not supported!
#endif /* __linux__ */