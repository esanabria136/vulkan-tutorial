#if __linux__ || __linux || __gnu_linux__
#ifndef UTILITIES_H_
#define UTILITIES_H_

#include <fstream>
#include <vector>
#include <vulkan/vulkan.h>
#include <glm/glm.hpp>

const size_t MAX_FRAME_DRAWS{ 2 };
const size_t MAX_OBJECTS{ 2 };

const std::vector<const char*> deviceExtensions =
{
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

struct Vertex
{
	glm::vec3 pos;	//Vertex position, X, Y, Z
	glm::vec3 col;
};

//indices of queue families if they exust at all
struct QueueFamilyIndices
{
	int graphicsFamily = -1;
	int presentationFamily = -1;

	//check if queue families are valid
	bool isValid()
	{
		return graphicsFamily >= 0 && presentationFamily >= 0;
	}
};

struct SwapChainDetails
{
	VkSurfaceCapabilitiesKHR surfaceCapabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentationModes;
};

struct SwapChainImage
{
	VkImage image;
	VkImageView imageView;
};

static std::vector<char> readFile(const std::string &filename)
{
	//open at end so we can know the size of the file since
	//the position is at the end, conveniently it is the file size
	std::ifstream file(filename, std::ios::binary | std::ios::ate);

	if(!file.is_open())
	{
		throw std::runtime_error("Failed to open a file: " + filename);
	}

	size_t fileSize{ static_cast<size_t>(file.tellg()) };
	std::vector<char> fileBuffer(fileSize);

	file.seekg(0);	//goto position 0 of the file

	file.read(fileBuffer.data(), fileSize);

	file.close();

	return fileBuffer;
}

static uint32_t findMemoryTypeIndex(VkPhysicalDevice physicalDevice, uint32_t allowedTypes, VkMemoryPropertyFlags properties)
{
	//Get properties of my physical device memory
	VkPhysicalDeviceMemoryProperties memoryProperties;
	vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);

	for(uint32_t i{ 0 }; i < memoryProperties.memoryTypeCount; ++i)
	{
		if((allowedTypes & (1 << i)) && ((memoryProperties.memoryTypes[i].propertyFlags & properties) == properties))
		{
			//this memory type is valid, so return its index
			return i;
		}
	}
}

static void createBuffer(
		VkPhysicalDevice physicalDevice,
		VkDevice device,
		VkDeviceSize bufferSize,
		VkBufferUsageFlags bufferUsage,
		VkMemoryPropertyFlags bufferProperties,
		VkBuffer *buffer,
		VkDeviceMemory *bufferMemory)
{
	//Information to create a buffer
		VkBufferCreateInfo bufferInfo = {};
		bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufferInfo.size = bufferSize;
		bufferInfo.usage = bufferUsage;
		bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		if(VkResult res = vkCreateBuffer(device, &bufferInfo, nullptr, buffer); res != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to create a Vertex Buffer: " + res);
		}

		//get buffer memory requirements
		VkMemoryRequirements memRequriements = {};
		vkGetBufferMemoryRequirements(device, *buffer, &memRequriements);

		//Allocate memory to buffer
		VkMemoryAllocateInfo memoryAllocateInfo = {};
		memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		memoryAllocateInfo.allocationSize = memRequriements.size;
		memoryAllocateInfo.memoryTypeIndex = findMemoryTypeIndex(physicalDevice, memRequriements.memoryTypeBits, bufferProperties);

		//Allocate memory to vkDeviceMemory
		if(VkResult res = vkAllocateMemory(device, &memoryAllocateInfo, nullptr, bufferMemory); res != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to allocate vertex buffer memory: " + res);
		}

		//Allocate memory to given vertex buffer
		vkBindBufferMemory(device, *buffer, *bufferMemory, 0);
}

static void copyBuffer(VkDevice device, VkQueue transferQueue, VkCommandPool transferCommandPool, VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize bufferSize)
{
	//command buffer to hold transfer commands
	VkCommandBuffer transferCommandBuffer;

	//command buffer details
	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandPool = transferCommandPool;
	allocInfo.commandBufferCount = 1;

	//Allocate command buffer from pool
	vkAllocateCommandBuffers(device, &allocInfo, &transferCommandBuffer);

	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	//Begin recording transfer commands
	vkBeginCommandBuffer(transferCommandBuffer, &beginInfo);

	//Region of data to copy from and to
	VkBufferCopy bufferCopyRegion = {};
	bufferCopyRegion.srcOffset = 0;
	bufferCopyRegion.dstOffset = 0;
	bufferCopyRegion.size = bufferSize;

	vkCmdCopyBuffer(transferCommandBuffer, srcBuffer, dstBuffer, 1, &bufferCopyRegion);

	vkEndCommandBuffer(transferCommandBuffer);

	//Queue submission information
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &transferCommandBuffer;

	//Submit transfer command to transfer queue and wait until it finishes
	vkQueueSubmit(transferQueue, 1, &submitInfo, VK_NULL_HANDLE);

	vkQueueWaitIdle(transferQueue);

	//Free temporary command buffer back to pool
	vkFreeCommandBuffers(device, transferCommandPool, 1, &transferCommandBuffer);
}


#endif /* UTILITIES_H_ */
#elif
#error Platform not supported!
#endif /* __linux__ */