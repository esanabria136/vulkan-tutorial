# Vulkan-Tutorial

Vulkan Tutorial

## Requirements

- [Vulkan SDK 1.2.135.0](https://vulkan.lunarg.com/sdk/home#sdk/downloadConfirm/1.2.135.0/linux/vulkansdk-linux-x86_64-1.2.135.0.tar.gz)
- [Ubuntu 20.04 64-bit](https://ubuntu.com/) 
- [Feral Gamemode](https://github.com/FeralInteractive/gamemode)
- [libSDL2](https://www.libsdl.org/)
- [GLM](https://glm.g-truc.net/0.9.9/index.html)
- C++ 2017

## Compilation
With g++

In `.profile` file `source /vulkan_sdk/1.2.135.0/setup-env.sh` as per the Vulkan documentation

Compile shaders inside the Shaders directory with the `glslangValidator` from the SDK

```
$ vulkan_sdk/1.2.135.0/x86_64/bin/glslangValidator -V shader.vert
$ vulkan_sdk/1.2.135.0/x86_64/bin/glslangValidator -V shader.frag
```

`FLAGS = -std=c++17 -march=native -Wno-return-type -Wno-narrowing`

`LIBS = -ldl -lSDL2 -lvulkan -L${VULKAN_SDK}/lib`

`INCL = -I${VULKAN_SDK}/include -include/gamemode/lib/gamemode_client.h`
